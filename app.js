var exports = module.exports = {};

var path = require('path');
var application_root = __dirname,
    express = require( 'express' ),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    errHandler = require('errhandler'),
    router = require('./routes/index.js');

//Create server
var app = express();
//parses request body and populates request.body
app.use( bodyParser.json() );

//checks request.body for HTTP method overrides
app.use( methodOverride('X-HTTP-Method-Override') );

//app.use(express.limit(100000000));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb'}));
//app.use( express.limit('4M'));
//perform route lookup based on url and HTTP method
app.use( express.Router() );

app.use(express.static(path.join(__dirname, 'public')));

errHandler.error(function(err, req, res, next){
  console.log('error occurred!!!');
  res.status(200).send({error: err.message});
});

app.use(errHandler.middleWare);

//Router
router.registerRouters(app);

//Start server
var port = 8182;
app.listen( port, function() {
  console.log( 'Express server listening on port %d in %s mode', port, app.settings.env );
});

