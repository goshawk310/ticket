var jwt = require('jwt-simple'),config = require('./config.js');

module.exports = function(req, res, next) {
  var token = req.query.authToken;
  console.log('token is: ' + token);
  var error = '';
  if (!token) {
    error = "This request is authenticated. Please provide a query param named [authToken].";
  }
  try {
    var decoded = jwt.decode(token, config.secret);

    if (decoded.exp <= Date.now() || !decoded.iss) {
      error = 'Access token has expired. Please relogin to issue a new token.';
    }

    req.user = decoded.iss;

  } catch (err) {
    error = "Authentication token is not valid. Please provide a valid authentication token.";
  }

  if(error) {
    res.send({success : false, errors : error});
  } else {
    return next();
  }

};