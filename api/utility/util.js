var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    nodemailer = require('nodemailer'),
    swig = require('swig');

var exports = module.exports = {};

// logger
var log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' },
    { type: 'file', filename: 'ticket-cloud.log', backups : 0, maxLogSize : 2048  }
  ]
});

exports.logger = log4js.getLogger();

// cryptography
exports.encrypt = function(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
exports.decrypt = function(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

// email http://www.nodemailer.com/
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'selahshor@gmail.com',
        pass: 'miselahshor'
    }
});


// random number generation
exports.randomString = function(length) {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

exports.sendMail = function(to, template, subject, datamap) {
    transporter.sendMail({
        to: to,
        subject: subject,
        html: swig.renderFile('./descriptors/templates/email/' + template, datamap)
    });
};