var eventDas, util;

var EventService = function Constructor(inEventDAS, inUserDAS, inUtil){eventDas = inEventDAS; userDas = inUserDAS, util = inUtil;};

EventService.prototype.createEvent = function (e, response) {
  util.logger.debug('adding new event');

  var callback1 = function(result) {
    util.logger.debug('add event callback1');
    if(result.success == false) {
      response.send(result);
      return;
    }

    value = result.e;
    response.send({success : true, event : {id : value.id}});
  }

  var callback2 = function(result) {
    util.logger.debug('add event callback2');
    util.logger.debug(result);
    if(result.success == false) {
      response.send(result);
      return;
    }

    var returned = result.e;
    if(returned) {
         response.send({success:false,errors:[{message:'user already has an event with same name'}]});
    } else {
      eventDas.create(e,callback1);
    }
  }

  eventDas.findEvent(e,callback2);
};

EventService.prototype.findById = function (inEvent, response) {
  util.logger.debug('finding event by id ' + inEvent.id);
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    value = result.e;

    if(value) {
      value = {id : value.id, 
        name : value.description, 
        address_id : value.address_id};
    } else {
      value = {success:false,errors:[{message:'event does not exist'}]};
    }

    response.send(value);
  }
  eventDas.get(inEvent, callback);
};

EventService.prototype.findAddressByEventId = function (inEvent, response) {
  util.logger.debug('finding event address by id ' + inEvent.id);
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    value = result.address;
    if(!value) {
      value = {success:false,errors:[{message:'event does not exist'}]};
    }

    response.send(value);
  }
  eventDas.getAddress(inEvent, callback);
};

EventService.prototype.updateEvent = function (e, response) {
    util.logger.debug('updating event ' + e);

    if(!e.id) {
      response.send({error : 'event id is required to update event'});
      return;
    }

    var callback = function(value) {
      if(value.success == false) {
        response.send(value);
        return;
      }

      var value = value.e;
      value = {id : value.id, 
        name : value.description, 
        address_id : value.address_id};

      response.send({success : true, user : value});
    }

    var cols = {};
    if(e.description) {
      cols['description'] = e.description;
    }

    eventDas.update(e, cols, callback);
};

EventService.prototype.updateAddress = function (address, response) {
    util.logger.debug('updating address ' + address.userId);

    if(!address.id) {
      response.send({error : 'event id is required to update event address'});
      return;
    }

    var callback = function(value) {
      if(value.success == false) {
        response.send(value);
        return;
      }
      response.send({success : true});
    }

    var cols = {};
    if(address.city) {
      cols['city'] = address.city;
    }
    if(address.state) {
      cols['state'] = address.state;
    }
    if(address.country) {
      cols['country_id'] = address.country;
    }

    eventDas.updateAddress(address, cols, callback);
};

EventService.prototype.deleteEvent = function (inEvent, response) {
  util.logger.debug('deleting event by id ' + inEvent.id);
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    value = {success:true, event : {id : result.e.id}};

    response.send(value);
  }
  eventDas.delete(inEvent, callback);
};

EventService.prototype.getTicketTypes = function (response) {
  util.logger.debug('getting ticket types');
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.types);
  }
  eventDas.findAllTicketTypes(callback);
};

EventService.prototype.getTicketCategories = function (userId, response) {
  util.logger.debug('getting ticket categories');
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.categories);
  }
  eventDas.findAllTicketCategories(userId,callback);
};

EventService.prototype.createTicketCategory = function (category, response) {
  util.logger.debug('adding new category');

  var callback1 = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    value = result.category;
    response.send({success : true, category : {id : value.id}});
  }

  var callback2 = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    var returned = result.category;
    if(returned) {
         response.send({success:false,errors:[{message:'user already has a ticket category with same name'}]});
    } else {
      eventDas.createTicketCategory(category,callback1);
    }
  }

  var callback0 = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    user = result.user;
    if(user.account_type_id == 1) {
     response.send({success:false,errors:[{message:'user can not create a new category. please upgrade for this feature'}]});
    } else {
      eventDas.findTicketCategory(category,callback2);
    }
  }

  userDas.get(category.userId,callback0);
};

EventService.prototype.deleteTicketCategory = function (category, response) {
  util.logger.debug('deleting ticket category by id ' + category.id);
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    value = {success:true, category : {id : result.category.id}};

    response.send(value);
  }

  var callback0 = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    user = result.user;
    if(user.account_type_id == 1) {
     response.send({success:false,errors:[{message:'user can not delete a category. please upgrade for this feature'}]});
    } else {
      eventDas.deleteTicketCategory(category, callback);
    }
  }

  userDas.get(category.userId,callback0);
};

EventService.prototype.mergeTicket = function (ticket, response) {
  util.logger.debug('adding new tcket');

  var callback = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    value = result.ticket;
    response.send({success : true, ticket : {id : value.id}});
  }

  var callback0 = function(result) {
    if(result.success == false) {
      response.send(result);
      return;
    }

    user = result.user;
    if(user.account_type_id == 1) {
      //emptying variable that free user can not set
      ticket.description = '';
      ticket.ticketTypeId = 1;
      ticket.ticketCategoryId = 0;
      ticket.seatId = '';
    }
    eventDas.createTicket(ticket,callback);
  }

  userDas.get(ticket.userId,callback0);
};

EventService.prototype.getTicket = function (ticket, response) {
  util.logger.debug('getting ticket by id');
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.ticket);
  }
  eventDas.findTicket(ticket, callback);
};

EventService.prototype.getTicketsByEvent = function (e, response) {
  util.logger.debug('getting tickets by event');
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.tickets);
  }

  var callback0 = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    console.log(result);
    console.log(e);
    if(result.e.account_id == e.userId) {
      eventDas.findTicketsByEvent(e.id, callback);
    } else {
      callback({success : false, errors:[{message:'no event for given id exist'}]});
    }
  }
  eventDas.get(e, callback0);
};

EventService.prototype.publishTicket = function (ticket, response) {
  util.logger.debug('publising ticket');
  var callback = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.ticket);
  }
  eventDas.findTicket(ticket, callback);
};

EventService.prototype.searchTickets = function (query, response) {
  util.logger.debug('searching ticket');
  var callback2 = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    response.send(result.tickets);
  }

  var callback1 = function(result) {

    if(result.success == false) {
      response.send(result);
      return;
    }
    query.events = result.events;
    eventDas.findTicketsByEventAndPublished(query, callback2);
  }
  eventDas.findEventsByAddressAndName(query, callback1);
};

module.exports = EventService;