var exports = module.exports = {};

var das = require('../das');
var util = require('../utility/util.js');

var UserManagementService = require('./usermanagement.js');
var EventService = require('./event.js');

exports.UserManagementService = function () {
  return new UserManagementService(das.UserDAS(), util);
};

exports.EventService = function () {
  return new EventService(das.EventDAS(), das.UserDAS(), util);
};