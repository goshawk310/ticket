var userDas;
var util;
var jwt = require('jwt-simple'),moment = require('moment'),config = require('../utility/config.js');

var UserManagementService = function Constructor(inUserDAS, inUtil){userDas = inUserDAS; util = inUtil;};

UserManagementService.prototype.authenticate = function (user, response) {

    var callback = function(result) {
        if(result.success == false) {
            response.send(result);
            return;
        }
        value = result.user;

        if(value && value.deleted == 0 && value.verified == true) {
            util.logger.debug('user ' + user.email + ' exist. verifying password');
            console.log(moment());
            if(value.password == util.encrypt(user.password)) {
                var expires = moment().add('days', 7).valueOf();
                var token = jwt.encode({ iss: value.id, exp: expires }, config.secret);
                value = {success:true, token : token, expires : expires, user : value.toJSON()};
            } else {
                value = {success:false, errors:[{message:'user was not authenticated'}]};
            }
        } else if(value && value.deleted == 1){
            value = {success:false, errors:[{message:'user account deactivated'}]};
        } else {
            util.logger.debug(errors);
            value = {success:false, errors:[{message:'user does not exist'}]};
        }
        response.send(value);
    }
    userDas.getByEmail(user.email, callback);
};

UserManagementService.prototype.findById = function (id, response) {
    util.logger.debug('finding user by id ' + id);
    var callback = function(result) {

        if(result.success == false) {
            response.send(result);
            return;
        }
        value = result.user;

        if(value && value.deleted == 0) {
            value = {email : value.email,
                firstName : value.firstName,
                lastName : value.lastName,
                accountType : value.account_type_id,
                verified : value.verified};
        } else if(value && value.deleted == 1){
            value = {success:false,errors:[{message:'user account deactivated'}]};
        } else {
            value = {success:false,errors:[{message:'user does not exist'}]};
        }

        response.send(value);
    }
    userDas.get(id, callback);
};

UserManagementService.prototype.addUser = function (user, response) {
    util.logger.debug('adding new user');

    if(user.password) {
        user.password = util.encrypt(user.password);
    }

    var callback = function(result) {
        util.logger.debug('add user callback');
        if(result.success == false)
        {
              response.send(result);
              return;
        }

        value = result.user;
        util.sendMail(user.email, 'verification.html', 'Signup Confirmation',
            {firstName : value.firstName, lastName : value.lastName, code : util.encrypt(user.email)});
        response.send({success : true, user : {email : value.email, id : value.id}});
    }

    userDas.create(user, callback);
};

UserManagementService.prototype.confirm = function (code, response) {
    util.logger.debug('confirming code ' + code);

    var email
    try {
        email = util.decrypt(code);
    } catch(e) {
        util.logger.debug('error occurreed while decoding code');
    }

    if(email) {
        var callback = function(result) {
            if(result.success == false) {
                response.send(result);
                return;
            }

            response.send({success : true});

        }

        userDas.confirm(email, callback);
    } else {
        response.send({success : false});
    }
};

UserManagementService.prototype.delete = function (id, flag, response) {
    util.logger.debug('deleting user ' + id);

    var callback = function(value) {
        if(value.success == false) {
            response.send(value);
            return;
        }
        response.send({success : true, userId : value.user.id});
    }

    userDas.delete(id, flag, callback);
};

UserManagementService.prototype.updateUser = function (user, response) {
    util.logger.debug('updating user ' + user);

    if(!user.email) {
        response.send({error : 'user id is required to update user'});
        return;
    }

    var callback = function(value) {
        if(value.success == false) {
            response.send(value);
            return;
        }

        var value = value.user;
        value = {email : value.email,
        details : value.details,
        phone_num : value.phone_num,
        first_name: value.first_name,
        last_name: value.last_name};

        response.send({success : true, user : value});
    }

    var cols = {};
    if(user.password) {
        cols['email'] = user.email;
    }
    if(user.phone_num) {
        cols['phone_num'] = user.phone_num;
    }
    if(user.details) {
        cols['details'] = user.details;
    }
    if(user.first_name){
        cols['firstName'] = user.first_name;
    }
    if(user.last_name){
        cols['lastName'] = user.last_name;
    }
    userDas.update(user.email, cols, callback);
};

UserManagementService.prototype.resetPassword = function (email, response) {
    util.logger.debug('reset user password');

    if(!email) {
        response.send({error : 'user email is required to reset user password'});
    }

    var password = util.randomString(7);

    var callback = function(value) {
        if(value.success == false) {
            response.send(value);
            return;
        }
        util.sendMail(email,'reset_password.html','Password Reset',{password : password});
        response.send({success : true});
    }
    userDas.resetPassword(email, util.encrypt(password), callback);
};

UserManagementService.prototype.updateAddress = function (address, response) {
    util.logger.debug('updating address ' + address.userId);

    if(!address.userId) {
        response.send({error : 'user id is required to update address'});
        return;
    }

    var callback = function(value) {
        if(value.success == false) {
            response.send(value);
            return;
        }
        response.send({success : true});
    }

    var cols = {};
    if(address.city) {
        cols['city'] = address.city;
    }
    if(address.state) {
        cols['state'] = address.state;
    }/*
    if(address.country) {
        cols['country_id'] = address.country;
    }*/

    userDas.updateAddress(address.userId, cols, callback);
};

UserManagementService.prototype.get_userProfile = function (user, response) {
    util.logger.debug('getting user_profile - ' + user.email);

    var callback = function (value) {
        if ( value.success ) {
            response.send({success: true, user_profile : value.user_profile});
        }
        else {
            response.send({success: false, errors : value.errors});
        }
    }

    userDas.getUserProfilebyEmail(user.email, callback);
}

UserManagementService.prototype.update_UserPhoto = function ( user_photo, response ){
    util.logger.debug('updating user_photo - ' + user_photo.email);

    var callback = function (value) {
        if ( value.success ) {
            response.send({success: true});
        }
        else {
            response.send({success: false});
        }
    }
    var cols = {};

    if(user_photo.photo_data) {
        cols['image_url'] = user_photo.photo_data;
    }

    userDas.update_UserPhoto(user_photo.email, cols, callback);
}

module.exports = UserManagementService;