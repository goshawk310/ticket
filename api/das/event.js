var Event, TicketType, Address, Ticket, TicketCategory, Seat;

var EventDAS = function Constructor(inEvent, inTicketType, inAddress, inTicket, inTicketCategory, inSeat, inCountry, insequelize){
	Event = inEvent;
	TicketType = inTicketType;
	Address = inAddress;
	Ticket = inTicket;
	TicketCategory = inTicketCategory;
	Seat = inSeat;
	Country = inCountry;
	sequelize = insequelize;
};

EventDAS.prototype.create = function (e, callback) {
	console.log('creating event 0');
	//get country and account type
	Promise.all([Country.find({ where: {iso2: 'AF'} })]).then(function(required) {
		if(required[0] == null) {
			callback({success : false, errors:[{message:'Country table has no data'}]});
		}	
		// create address
		Address.create({country_id : required[0].id}).then(function(rAddress) {

			var newEvent = {description : e.description, address_id : rAddress.id, account_id : e.userId};
			Event.create(newEvent).then(function(rEvent) {
	  			callback({success : true, e : rEvent});
	  		}).catch(function(errors) {
				callback({success : false, errors : errors.errors});
			})
			

		}).catch(function(errors) {
  			callback({success : false, errors : errors.errors});
  		})

	})
};

EventDAS.prototype.findEvent = function (e, callback) {
	Event.find({ where: {description: e.description, account_id : e.userId} }).then(function(rEvent) {

		callback({success : true, e : rEvent});
	}).catch(function(errors) {
		console.log(errors);
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.update = function (e, cols, callback) {
	Event.find({ where: {id: e.id, account_id : e.userId} }).then(function(rEvent) {
	  if (rEvent) { // if the record exists in the db
	    rEvent.updateAttributes(cols).success(function() {});
	    callback({success : true, e : rEvent});
	  } else {
	  	callback({success : false});
	  }
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.updateAddress = function (a, cols, callback) {
	Event.find({ where: {id: a.id,account_id : a.userId},include: [{ model : Address, as : 'Address' }]}).then(function(rEvent) {
	  if (rEvent) { // if the record exists in the db
	    rEvent.Address.updateAttributes(cols).success(function() {});
	    callback({success : true, e : rEvent});
	  } else {
	  	callback({success : false});
	  }
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.get = function (e, callback) {
	Event.find({ where: {id: e.id, account_id : e.userId} }).then(function(rEvent) {
		callback({success : true, e : rEvent});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.getAddress = function (e, callback) {
	Event.find({ where: {id: e.id,account_id : e.userId},include: [{ model : Address, as : 'Address' }]}).then(function(rEvent) {
	  if(rEvent && rEvent.Address && rEvent.Address.dataValues) {
	    callback({success : true, address : rEvent.Address.dataValues});
	  } else {
	  	callback({success : true});
	  }
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.delete = function (e, callback) {
	Event.find({ where: {id: e.id, account_id : e.userId} }).then(function(rEvent) {
		rEvent.destroy();
		callback({success : true, e : rEvent});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.findAllTicketTypes = function (callback) {
	TicketType.findAll().then(function(ticketTypes) {
		callback({success : true, types : ticketTypes});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.findAllTicketCategories = function (userId, callback) {
	TicketCategory.findAll({ where: {account_id: userId} }).then(function(ticketCategories) {
		callback({success : true, categories : ticketCategories});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.findTicketCategory = function (category, callback) {
	TicketCategory.find({ where: {description: category.description, account_id : category.userId} }).then(function(rCategory) {
		callback({success : true, category : rCategory});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.createTicketCategory = function (category, callback) {
	var newTC = {description : category.description, account_id : category.userId};

	TicketCategory.create(newTC).then(function(rTC) {
			callback({success : true, category : rTC});
		}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.deleteTicketCategory = function (category, callback) {
	TicketCategory.find({ where: {id: category.id, account_id : category.userId} }).then(function(rCategory) {
		rCategory.destroy();
		callback({success : true, category : rCategory});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.createTicket = function (ticket, callback) {
	var promises = [];
	promises.push(Event.find({ where: {id: ticket.eventId, account_id : ticket.userId} }));
	promises.push(TicketType.find({ where: {id: ticket.ticketTypeId}}));

	if(ticket.ticketCategoryId == 0) {
		promises.push(TicketCategory.find({ offset: 0, limit: 1,order: 'id',where: {account_id: ticket.userId}}));
	} else {
		promises.push(TicketCategory.find({ where: {id: ticket.ticketCategoryId, account_id : ticket.userId}}));
	}

	if(ticket.seatId) {
		promises.push(Seat.find({ where: {id: ticket.seatId, event_id : ticket.eventId}}));
	}

	//get country and account type
	Promise.all(promises).then(function(required) {
		var dao = {description : ticket.description, fee : ticket.fee}
		if(ticket.seatId) {
			if(required[3] == null) {
				callback({success : false, errors:[{message:'no seat for given id exist'}]});
				return;
			} else {
				dao.seat_id = required[3].id;
			}
		}

		if(required[0] == null) {
			callback({success : false, errors:[{message:'no event for given id could be found'}]});
			return;
		} else {
			dao.event_id = required[0].id;
		}

		if(required[1] == null) {
			callback({success : false, errors:[{message:'no ticket type for given id could be found'}]});
			return;
		} else {
			dao.ticket_type_id = required[1].id;
		}

		if(required[2] == null) {
			callback({success : false, errors:[{message:'no ticket category for given id could be found'}]});
			return;
		} else {
			dao.ticket_category_id = required[2].id;
		}

		if(ticket.id) {
			Ticket.find({ where: {id: ticket.id},include: [{ model : Event, as : 'Event' }]}).then(function(rTicket) {
			 if (rTicket && rTicket.Event && rTicket.Event.account_id == ticket.userId) { // if the record exists in the db and belongs to same user
			    
			    var cols = {};
			    cols['event_id'] = dao.event_id;
			    cols['ticket_type_id'] = dao.ticket_type_id;
			    cols['ticket_category_id'] = dao.ticket_category_id;
			    cols['description'] = dao.description;
			    cols['fee'] = dao.fee;

			    if(dao.seat_id) {
			    	cols['seat_id'] = dao.fee;
			    }

			    rTicket.updateAttributes(cols);
			    callback({success : true, ticket : rTicket});
			  } else {
			  	callback({success : false});
			  }
			}).catch(function(errors) {
				console.log('error occurred here');
				console.log(errors);
				callback({success : false, errors : errors.errors});
			})
		} else {
			// create ticket
			Ticket.create(dao).then(function(rTicket) {
		  		callback({success : true, ticket : rTicket});
			}).catch(function(errors) {
	  			callback({success : false, errors : errors.errors});
	  		})
		}

	})
};

EventDAS.prototype.findTicket = function (ticket, callback) {
	Ticket.find({ where: {id: ticket.id},include: [{ model : Event, as : 'Event' }]}).then(function(rTicket) {
		if (rTicket && rTicket.Event && rTicket.Event.account_id == ticket.userId) {
			callback({success : true, ticket : rTicket});
		} else {
			callback({success : false, errors:[{message:'ticket with given id was not found'}]});
		}
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.findTicketsByEvent = function (eventId, callback) {
	Ticket.findAll({ where: {event_id: eventId}}).then(function(rTicket) {
		callback({success : true, tickets : rTicket});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.publishTicket = function (ticket, callback) {
	Ticket.find({ where: {id: ticket.id},include: [{ model : Event, as : 'Event' }]}).then(function(rTicket) {
		if (rTicket && rTicket.Event && rTicket.Event.account_id == ticket.userId) {
			rTicket.updateAttributes({is_published : true});
			callback({success : true, ticket : rTicket});
		} else {
			callback({success : false, errors:[{message:'ticket with given id was not found'}]});
		}
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

EventDAS.prototype.findEventsByAddressAndName = function (query, callback) {
	sequelize.query('select e.id from event e inner join address a on a.id = e.address_id inner join country c on a.country_id = c.id where e.description ilike ? OR a.city ilike ? OR a.state ilike ? OR c.name ilike ?', 
	  { replacements: ['%'+query.eventName+'%','%'+query.city+'%','%'+query.state+'%','%'+query.country+'%'], type: sequelize.QueryTypes.SELECT }
	).then(function(events) {
	  var ids = [];
	  for(var i = 0; i < events.length; i++) {
	  	ids.push(events[i].id);
	  }
	  callback({success : true, events : ids});
	});
};

EventDAS.prototype.findTicketsByEventAndPublished = function (query, callback) {
	Ticket.findAll({ where: sequelize.or({event_id: query.events},{published: query.published})}).then(function(rTicket) {
		callback({success : true, tickets : rTicket});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

module.exports = EventDAS;