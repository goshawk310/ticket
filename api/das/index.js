var exports = module.exports = {};

var UserDAS = require('./user.js');
var EventDAS = require('./event.js');

var Sequelize = require('sequelize');
var config = require('../utility/config.js');
var sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  dialect: config.dialect,

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var AccountType = sequelize.define('AccountType', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  type: { field: 'type', type: Sequelize.STRING, unique : true, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'account_type'});

var Country = sequelize.define('Country', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  iso2: { field: 'iso2', type: Sequelize.STRING, unique : true, allowNull : false},
  iso3: { field: 'iso3', type: Sequelize.STRING, unique : true, allowNull : false},
  isoCode: { field: 'iso_code', type: Sequelize.STRING, unique : true, allowNull : false},
  name: { field: 'name', type: Sequelize.STRING, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'country'});

var Address = sequelize.define('Address', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  city: { field: 'city', type: Sequelize.STRING},
  state: { field: 'state', type: Sequelize.STRING}
},{createdAt: false,updatedAt: false, tableName : 'address'});

var User = sequelize.define('User', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  email: { field: 'email', type: Sequelize.STRING, unique: true, allowNull : false, validate: {isEmail : true}},
  password: { field: 'password', type: Sequelize.STRING, allowNull : false},
  firstName: { field: 'first_name', type: Sequelize.STRING, allowNull : false},
  lastName: { field: 'last_name', type: Sequelize.STRING, allowNull : false},
  phone_num: {field: 'phone_num', type: Sequelize.STRING},
  details: {field: 'details', type: Sequelize.TEXT},
  image_url: {field: 'image_url', type: Sequelize.TEXT},
  verified: { field: 'is_verified', type: Sequelize.BOOLEAN, defaultValue: false},
  deleted: { field: 'deleted', type: Sequelize.BIGINT, defaultValue: 0}
},{createdAt: false,updatedAt: false, tableName : 'person'});

var Event = sequelize.define('Event', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  description: { field: 'description', type: Sequelize.STRING, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'event'});

var TicketType = sequelize.define('TicketType', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  type: { field: 'type', type: Sequelize.STRING, unique : true, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'ticket_type'});

var TicketCategory = sequelize.define('TicketCategory', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  description: { field: 'description', type: Sequelize.STRING, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'ticket_category'});

var Seat = sequelize.define('Seat', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  seatNumber: { field: 'seat_number', type: Sequelize.STRING, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'seat'});

var Ticket = sequelize.define('Ticket', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  fee: { field: 'fee', type: Sequelize.BIGINT, allowNull : false},
  description: { field: 'description', type: Sequelize.STRING, allowNull : false},
  published: { field: 'is_published', type: Sequelize.BOOLEAN, defaultValue: false, allowNull : false},
  sold: { field: 'is_sold', type: Sequelize.BOOLEAN, defaultValue: false, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'ticket'});

var UserAccess = sequelize.define('UserAccess', {
  id: { field: 'id', type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
  accessLevel: { field: 'access_level', type: Sequelize.BIGINT, allowNull : false}
},{createdAt: false,updatedAt: false, tableName : 'user_access'});

Address.belongsTo(Country, {foreignKey: {name: 'country_id', allowNull: false }});

User.belongsTo(AccountType, {foreignKey: {name: 'account_type_id', allowNull: false }});
User.belongsTo(Address, {foreignKey: {name: 'address_id', allowNull: false }});

Event.belongsTo(User, {foreignKey: {name: 'account_id', allowNull: false }});
Event.belongsTo(Address, {foreignKey: {name: 'address_id', allowNull: false }});

TicketCategory.belongsTo(User, {foreignKey: {name: 'account_id', allowNull: false }});

Seat.belongsTo(Event, {foreignKey: {name: 'event_id', allowNull: false }});

Ticket.belongsTo(Event, {foreignKey: {name: 'event_id', allowNull: false }});
Ticket.belongsTo(TicketType, {foreignKey: {name: 'ticket_type_id', allowNull: false }});
Ticket.belongsTo(TicketCategory, {foreignKey: {name: 'ticket_category_id', allowNull: false }});
Ticket.belongsTo(Seat, {foreignKey: {name: 'seat_id', allowNull: true }});

UserAccess.belongsTo(User, {foreignKey: {name: 'user_id', allowNull: false }});
UserAccess.belongsTo(User, {foreignKey: {name: 'account_id', allowNull: false }});

//synchronize all tables
sequelize.sync();

exports.UserDAS = function () {
  return new UserDAS(User, AccountType, Address, Country,TicketCategory);
};

exports.EventDAS = function () {
  return new EventDAS(Event, TicketType, Address, Ticket, TicketCategory, Seat, Country, sequelize);
};