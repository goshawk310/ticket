var User, AccountType, Address, Country, TicketCategory;

var UserDAS = function Constructor(inUser, inAccountType, inAddress, inCountry, inTicketCategory){
	User = inUser;
	AccountType = inAccountType;
	Address = inAddress;
	Country = inCountry;
	TicketCategory = inTicketCategory;
};

UserDAS.prototype.getByEmail = function (email, callback) {
	User.find({ where: {email: email} }).then(function(user) {
		callback({success : true, user : user});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.getUserProfilebyEmail = function (email, callback) {
	User.find({ where: {email: email} }).then(function(user) {
		Address.find({ where: {id: user.address_id}}).then(function(address){
			var user_profile = {
				email: user.email,
				password: user.passward,
				first_name: user.firstName,
				last_name: user.lastName,
				details: user.details,
				image_url: user.image_url,
				city: address.city,
				state: address.state,
				phone_num: user.phone_num,
				upcoming_event: 16,
				passed_event: 15,
				address_id: address.id
			}
			console.log(user.image_url);
			callback({success: true, user_profile : user_profile})
		}).catch(function(errors){
			callback({success: false, errors : errors.errors})
		})
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.get = function (id, callback) {
	User.find({ where: {id: id} }).then(function(user) {
		callback({success : true, user : user});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.create = function (user, callback) {

	console.log('-------UserDAS.prototype.crate - first');
	//get country and account type
	Promise.all([Country.find({ where: {iso2: 'AF'} }), AccountType.find({ where: {type: 'FREE'} })]).then(function(required) {
		console.log('-------UserDAS.prototype.crate - Promise.all');
		if(required[0] == null) {
			callback({success : false, errors:[{message:'Country table has no data'}]});
		}	

		if(required[1] == null) {
			callback({success : false, errors:[{message:'AccountType table has no data'}]});
		}			
		// create address
		Address.create({country_id : required[0].id}).then(function(rAddress) {

			user.address_id = rAddress.id;
			user.account_type_id = required[1].id;
			user.first_name = "XXX";
			user.image_url = "images/temp/usertemp.png";
			user.last_name = "YYY";
			user.phone_num = "????-???"

			console.log('-------UserDAS.prototype.crate - Promise.all - Address.create - first : address_id = '
				, rAddress.id, ' : account_type_id = ', required[1].id, ' : name = ', user.first_name, ' '
				, user.last_name,'---------');

			User.create({email: user.email,
						password: user.password,
						firstName: user.first_name,
						lastName: user.last_name,
						image_url: user.image_url,
				  		phone_num: user.phone_num,
						address_id: user.address_id,
						account_type_id: user.account_type_id})
			.then(function(rUser) {
				console.log('-------UserDAS.prototype.crate - Promise.all - Address.create - User.create');
				//create default ticket category for the ticket
				TicketCategory.create({description : 'Default', account_id : rUser.id}).then(function(rTicketCategory) {
					console.log('-------UserDAS.prototype.crate - Promise.all - Address.create - TicketCategory.create');
					callback({success : true, user : rUser});
				}).catch(function(errors) {
					console.log(errors);
					callback({success : false, errors : errors.errors});
				})
	  		}).catch(function(errors) {
				callback({success : false, errors : errors.errors});
			})
			//User.create(user);

		}).catch(function(errors) {
  			callback({success : false, errors : errors.errors});
  		})

	})
};

UserDAS.prototype.confirm = function (email, callback) {
	User.find({ where: {email: email} }).then(function(user) {
	  if (user) { // if the record exists in the db
	    user.updateAttributes({
	      verified: true
	    }).success(function() {});
	    callback({success : true, user : user});
	  } else {
	  	callback({success : false});
	  }
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.delete = function (id, flag, callback) {
	User.find({ where: {id: id} }).then(function(user) {
		if (user) { // if the record exists in the db
		user.updateAttributes({
		  deleted: flag
		}).success(function() {});
		callback({success : true, user : user});
		} else {
		callback({success : false});
		}
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.update = function (vEmail, cols, callback) {
	User.find({ where: {email: vEmail} }).then(function(user) {
		if (user) { // if the record exists in the db
			user.updateAttributes(cols);
			callback({success : true, user : user});
		} else {
			callback({success : false});
		}
	}).catch(function(errors) {
		console.log(errors);
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.updateAddress = function (userId, cols, callback) {
	Address.find({ where: {id: userId} }).then(function(address) {
		if (address) { // if the record exists in the db
			address.updateAttributes(cols);
			console.log('------------', cols);
			callback({success : true, address : address});
		} else {
			callback({success : false});
		}
	}).catch(function(errors) {
		console.log(errors);
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.resetPassword = function (email, password, callback) {
	User.find({ where: {email: email} }).then(function(user) {
	  if (user) { // if the record exists in the db
	    user.updateAttributes({
	      password: password
	    }).success(function() {});
	    callback({success : true, user : user});
	  } else {
	  	callback({success : false});
	  }
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
};

UserDAS.prototype.update_UserPhoto = function (email, cols, callback) {
	User.find({ where: {email: email} }).then(function(user) {
		user.updateAttributes(cols).success(function() {
			callback({success:true});
		});
	}).catch(function(errors) {
		callback({success : false, errors : errors.errors});
	})
}

module.exports = UserDAS;