Ticket_on_Cloud.controller('featuredController', ['$scope', '$rootScope', '$location', 'notificationConfigService', 'notify', function ($scope, $rootScope, $location, notificationConfigService, notify) {
    $scope.init = function() {

        $scope.smokes = [
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS1', subTitle : 'Imagine Dragons1', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS2', subTitle : 'Imagine Dragons2', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS3', subTitle : 'Imagine Dragons3', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS4', subTitle : 'Imagine Dragons4', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' },
            { img_url : 'images/temp/concerttemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5', location : 'Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000' }
        ]
        $scope.categories = [
            { name : 'ROCK AND POP' },
            { name : 'ALTERNATIVE ROCK' },
            { name : 'CLASSCIAL' },
            { name : 'DANCE/ELECTRONIC' },
            { name : 'JAZZ AND BULUES' },
            { name : 'HARD ROCK ' }
        ]
    }

    $scope.init();

}]).value( "Smoke", React.createClass( {
    propTypes: {
        data: React.PropTypes.object.isRequired
    },

    render: function() {
        return React.DOM.div( null,
            React.DOM.img({ src : this.props.data.img_url, alt : this.props.data.title}),
            React.DOM.h4( null, this.props.data.title),
            React.DOM.h5( null, this.props.data.subTitle),
            React.DOM.p( null, this.props.data.location ),
            React.DOM.p( null, this.props.data._daytime ),
            React.DOM.p( null, this.props.data.price ),
            React.DOM.button( {className : 'btn btn-default col-md-5 col-md-offset-3', type : 'submit'}, 'Details' ),
            React.DOM.p()
        );
    }
})).directive( 'smoke', function( reactDirective ) {
    return reactDirective( 'Smoke' );
}).value( "Category", React.createClass( {
    propTypes: {
        data: React.PropTypes.object.isRequired
    },

    render: function() {
        return React.DOM.div( null,
            React.DOM.button({className : 'btn btn-submit btn-block', type : 'submit'}, this.props.data.name)
        );
    }
})).directive( 'category', function( reactDirective ) {
    return reactDirective( 'Category' );
});

