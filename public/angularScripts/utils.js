﻿var utils = function utils() { };
utils.cookieName = 'sm-user';
// Globally setting the rest server URL
utils.restServer = 'http://localhost:8182/';
utils.rest = {
    signUp : utils.restServer + 'user/signup',
    signIn : utils.restServer + 'user/signin',
    user_Profile : utils.restServer + 'user/get_userprofile',
    update_User : utils.restServer + 'user/update_user',
    update_Address: utils.restServer + 'user/address/update',
    update_Photo: utils.restServer + 'user/update_photo'
}