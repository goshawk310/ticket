Ticket_on_Cloud.controller('UserManagementController', ['$scope', '$rootScope', '$location', '$route', 'UserManagementService', 'UserValidationService', 'notificationConfigService', 'notify', function ($scope, $rootScope, $location, $route,UserManagementService, UserValidationService, notificationConfigService, notify) {
    /* --------------------------------------- Sign In ----------------------------------- */

    //Setting the configuration for notification messages.
    notificationConfigService.notifyConfig(notify);
    notify.closeAll();
    //Event on the submission of login form
    $scope.signIn = function () {
        var user = $scope.user;
        notify.closeAll();

        var validationResult= UserValidationService.validateForm(user) ;

        debugger
        if (validationResult.email == false) {
            notify({
                message: "Email Id is not valid",
                classes: 'cg-notify-error-message'
            });
            $scope.user.email = "";
            $scope.emailFocus = true;
        } else
            signIn($scope.user.email, $scope.user.password);
    }

    //SignIn function that will call the signup method in userService.
    var signIn = function (email, password) {
        UserManagementService.signIn(email, password)
        .then(function (result) {
            debugger
            if (result.success) {
                notify({
                    message: result.message,
                    classes: 'cg-notify-success-message'
                });
                $location.path('/featured');
            }
            else {
                notify({
                    message: result.message,
                    classes: 'cg-notify-error-message'
                });
            }
        });
    }


    /* -------------------------------------- Sing up --------------------------------- */
    /*scope initialize function*/
    $scope.signup_init = function() {
        FB.init({ // this line replaces FB.init({
            appId      : '928213777258316', // Facebook App ID
            cookie     : false,
            xfbml      : true  // initialize Facebook social plugins on the page
        });

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    };

    //Event on the submission of login form
    $scope.addUser = function () {
        var user = $scope.user;
        notify.closeAll();
        var validationResult= UserValidationService.validateForm(user) ;

        if (validationResult.email == false) {
            notify({
                message: "Email Id is not valid",
                classes: 'cg-notify-error-message'
            });
            $scope.user.email = "";
            $scope.emailFocus = true;
        }
        else {
            if (validationResult.cfmPassword == false) {
                notify({
                    message: "Password and Confirm password should be same",
                    classes: 'cg-notify-error-message'
                });
                $scope.user.password = "";
                $scope.user.confirmpassword = "";
                $scope.passwordFocus = true;
            }
            else {
                signUp($scope.user.email, $scope.user.password);
            }
        }
    };

    $scope.fbSignup = function() {
        FB.login(function(response) {
            if (response.authResponse) {
                //var aaa = getUserInfo(); // Get User Information.
                alert("success!");
                console.log(response.authResponse);
            }
            else {
                alert('Authorization failed.');
            }
        });
    }

    $scope.googleSignup = function() {
        //google+ login callback function
        var loginFinished = function(authResult) {
            debugger
            var useremail = '', userpassword='';
            if (authResult['status']['signed_in']) {
                gapi.client.load('oauth2', 'v2', function() {
                    gapi.client.oauth2.userinfo.get().execute(function(resp){
                        debugger
                        useremail = resp.email; //get user email
                        userpassword = 'google!(()psw';
                        console.log("-----------Useremail = " + useremail + " : Userpassword = " + userpassword + "-------------");
                        this.signUp(useremail, userpassword)
                        .then(function (result) {
                            debugger
                            if (result.success) {
                                notify({
                                    message: result.message,
                                    classes: 'cg-notify-success-message'
                                });
                            }
                            else {
                                notify({
                                    message: result.message,
                                    classes: 'cg-notify-error-message'
                                });
                            }
                        });
                    });
                });
            } else {
                alert("Google connection error!");
            }
        };

        var options = {
            'callback' : loginFinished,
            'approvalprompt' : 'force',
            'clientid' : '1073139336620-dsa5c64mo1ej9o1k9sb1nsf77gp7bpbf.apps.googleusercontent.com',
            'requestvisibleactions' : 'http://schema.org/CommentAction http://schema.org/ReviewAction',
            'cookiepolicy' : 'single_host_origin',
            'scope': 'profile email'
        };

        gapi.auth.signIn(options);
    };

    //SignUp function that will call the signup method in userService.
    var signUp = function (email, password) {
        UserManagementService.signUp(email, password)
        .then(function (result) {
            debugger
            if (result.success) {
                notify({
                    message: result.message,
                    classes: 'cg-notify-success-message'
                });
            }
            else {
                notify({
                    message: result.message,
                    classes: 'cg-notify-error-message'
                });
            }
        });
    }


    /* ---------------------------------- Update --------------------------------- */


    $scope.update_init = function() {
        $scope.fileChanged = function(e) {

            var files = e.target.files;

            var fileReader = new FileReader();
            fileReader.readAsDataURL(files[0]);

            fileReader.onload = function(e) {
                $scope.imgSrc = this.result;
                $scope.$apply();
            };

        }

        $scope.clear = function() {
            $scope.imageCropStep = 1;
            delete $scope.imgSrc;
            delete $scope.result;
            delete $scope.resultBlob;
        };

        UserManagementService.getCur_UserProfile().then(function(result)
        {
            if ( result.success ) {

                $scope.users = [
                    {
                        address_id: result.data.address_id,
                        img_url: result.data.image_url,
                        first_name: result.data.first_name,
                        last_name: result.data.last_name,
                        events_cnt: 42,
                        details: result.data.details,
                        city: result.data.city,
                        state: result.data.state,
                        phone_num: result.data.phone_num,
                        email: result.data.email
                    }
                ];
            }
        });

        $scope.eventbuts = [
            { but_name: 'UPCOMING EVENTS', events_cnt : 15},
            { but_name: 'PASSED EVENTS', events_cnt : 15}
        ];
        $scope.posters = [
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS1', subTitle : 'Imagine Dragons1',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS2', subTitle : 'Imagine Dragons2',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS3', subTitle : 'Imagine Dragons3',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS4', subTitle : 'Imagine Dragons4',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            },
            { img_url : 'images/temp/postertemp.png', title : 'SMOKE + MIRRORS5', subTitle : 'Imagine Dragons5',
                location : 'Cross Club, Prague, Czech', _daytime : 'Friday, 28 Jun 15 - 19:00pm', price : 'From $200 to $1,000',
                details : 'Lorem ipsum amet integer non arcu pellentesque ut urna nec curabitur. \n' +
                'Nam mauirs porta tempus molestie proin ut gravida, at congue malesuada, at, gravida, ultricies' +
                ' lectus vivams porta molestie porttitor tellus vulputate metus mattis arcu. Donec sit nibh ' +
                'pellenteque spien porta actuor...'
            }
        ]

        $scope.userID = "";
    }


  //  $scope.update_init();

    $scope.user_edit = function() {
        $scope.userID = jQuery(".user-edit").val();
    }

    $scope.onupdate_profile = function() {

        $scope.users[0].details = jQuery(".details_input").val();
        $scope.users[0].email = jQuery(".email_input").val();
        $scope.users[0].phone_num = jQuery(".phone_input").val();
        $scope.users[0].city = jQuery(".city_input").val();
        $scope.users[0].state = jQuery(".state_input").val();
        $scope.users[0].first_name = jQuery(".first_name_input").val();
        $scope.users[0].last_name = jQuery(".last_name_input").val();

        jQuery('#myModal').on('hidden.bs.modal', function (e) {
            // do something...
            $route.reload();
        })
        jQuery("#myModal").modal('hide');
        UserManagementService.update_UserProfile({first_name: $scope.users[0].first_name, last_name: $scope.users[0].last_name,
            address_id: $scope.users[0].address_id, email: $scope.users[0].email,
            phone_num: $scope.users[0].phone_num, address: $scope.users[0].address, details: $scope.users[0].details,
            city: $scope.users[0].city, state: $scope.users[0].state}).then(function(result){
            if (result.success == true){
                notify({
                    message: 'Update Succss!',
                    classes: 'cg-notify-success-message'
                });
            }
            else {
                notify({
                    message: 'Update Error!',
                    classes: 'cg-notify-error-message'
                });
            }
        });
    }

    $scope.onphoto_update = function() {
        jQuery('#myPhotoModal').on('hidden.bs.modal',function(e){
            $route.reload();
        })
        jQuery('#myPhotoModal').modal('hide');
        UserManagementService.update_UserPhoto({photo_data: $scope.result}).then(function(result){
            if (result.success == true){
                notify({
                    message: 'Update Succss!',
                    classes: 'cg-notify-success-message'
                });
            }
            else {
                notify({
                    message: 'Update Error!',
                    classes: 'cg-notify-error-message'
                });
            }
        });
    };
}]).value( "User", React.createClass( {
    propTypes: {
        data: React.PropTypes.object.isRequired
    },
    switch_angular: function() {
        jQuery(".user-edit").val(this.props.data.email);
        jQuery(".user-edit").trigger("click");
    },
    switch_photo: function() {
        //jQuery(".photo-edit");
        jQuery(".photo-edit").trigger("click");
    },
    render: function() {

        return React.DOM.div( {className : 'user-div'},
            React.DOM.div( {className: 'col-md-4'},
                React.DOM.img({ src : this.props.data.img_url, className: 'full-div-img'}),
                React.DOM.div( {className: 'col-md-12 fileinputs'},
                    React.DOM.input( { className: 'col-md-12', type: "file", id: "fileupload-example-4" } ),
                    React.DOM.label( {className: 'form-control col-md-12 btn btn-submit upload-but-label',
                        id : "fileupload-example-4-label", for: "fileupload-example-4" ,
                        "data-toggle" : "modal", "data-target" :"#myPhotoModal", onClick : this.switch_photo}, 'Upload')
                )
                // React.DOM.a({className: 'btn btn-submit upload-btn', type : 'submit'}, 'UPLOAD PHOTO')
            ),
            React.DOM.div( {className: 'col-md-8'},
                React.DOM.div( {className: 'half-height-div'},
                    React.DOM.div( null ,
                        React.DOM.h4( {className: 'col-md-9 col-xs-8 inline-block-div'},
                            this.props.data.first_name + ' ' + this.props.data.last_name),
                        React.DOM.button( {className: 'btn btn-submit col-md-3 col-xs-4', "data-toggle" : "modal",
                            "data-target" :"#myModal",onClick : this.switch_angular}, 'Edit')
                    ),
                    React.DOM.p(null, 'Events visited: ' + this.props.data.events_cnt),
                    React.DOM.p(null, this.props.data.details)
                ),
                React.DOM.div( {className: 'half-height-div'},
                    React.DOM.p(null, 'Address: ' + this.props.data.city + ', ' + this.props.data.state),
                    React.DOM.p(null, 'Phone: ' + this.props.data.phone_num),
                    React.DOM.p(null, 'Email: ' + this.props.data.email)
                )
            )
        );
    }
})).directive( 'user', function( reactDirective ) {
    return reactDirective( 'User' );
}).value( "Eventbut", React.createClass( {
    propTypes: {
        data: React.PropTypes.object.isRequired
    },

    render: function() {
        return React.DOM.div( {className : 'eventbuts-div col-md-4'},
            React.DOM.button( {className : 'btn btn-submit btn-block'},
                this.props.data.but_name + ' (' + this.props.data.events_cnt + ')'
            )
        );
    }
})).directive( 'eventbut', function( reactDirective ) {
    return reactDirective( 'Eventbut' );
}).value( "Poster", React.createClass( {
    propTypes: {
        data: React.PropTypes.object.isRequired
    },

    render: function() {
        return React.DOM.div( {className : 'posters-div'},
            React.DOM.div( {className : 'space-div col-md-12'}),
            React.DOM.div( {className : 'bottom-bold-border-div col-md-12'} ),
            React.DOM.div( {className : 'space-div col-md-12'}),
            React.DOM.div( {className : 'col-md-4'},
                React.DOM.img({ src : this.props.data.img_url, className: 'full-div-img'})
            ),
            React.DOM.div( {className : 'col-md-7'},
                React.DOM.h5(null, this.props.data.title + ' by ' + this.props.data.subTitle),
                React.DOM.p(null, this.props.data.location),
                React.DOM.p(null, this.props.data._daytime),
                React.DOM.p(null, 'Origanizer: ' + 'EVENT OWNER'),
                React.DOM.p(null, 'Price: ' + this.props.data.price),

                React.DOM.div( {className : 'bottom-light-border-div'} ),

                React.DOM.p(null, this.props.data.details)
            )
        );
    }
})).directive( 'poster', function( reactDirective ) {
    return reactDirective( 'Poster' );
})