Ticket_on_Cloud.service('UserManagementService', ['$q', '$resource', '$cookieStore', function ( $q, $resource, $cookieStore) {

    /* ------------------------------------ Sign in --------------------------------- */

    this.signIn = function (vEmail, vPassword) {
        var user = $resource(utils.rest.signIn);
        var deferred = $q.defer();
        user.save({email: vEmail, password: vPassword})
            .$promise.then(function (result) {
                debugger
                if (result.success == true) {
                    $cookieStore.put('cur_user', result.user);
                    deferred.resolve({ success: true, message:'Successfully Logged In'});
                }
                else {
                    deferred.resolve({ success: false, message:'Invalid Credentials' });
                }
            }, function (error) {debugger });
        return deferred.promise;
    }

    /* ------------------------------------- Sign up ---------------------------------- */

    this.signUp = function (vEmail, vPassword) {
        var user = $resource(utils.rest.signUp);
        var deferred = $q.defer();
        user.save({email: vEmail, password: vPassword})
            .$promise.then(function (result) {
                debugger
                if (result.success == true) {
                    deferred.resolve({ success: true, message:'An email has been sent to your mail Id for verification.' });
                }
                else {
                    deferred.resolve({ success: false, message:'Email Id already used.' });
                }
            }, function (error) {debugger });

        return deferred.promise;
    }

    /* ------------------------------------- Update ------------------------------------*/

    this.getCur_UserProfile = function() {
        var deferred = $q.defer();
        var cur_user = $cookieStore.get('cur_user');
        var user_profile = $resource(utils.rest.user_Profile);
        user_profile.save({email : cur_user.email, password : cur_user.password})
            .$promise.then(function (result) {
                debugger
                if (result.success == true) {
                    deferred.resolve({ success: true, data : result.user_profile});
                }
            }, function (error) {
                deferred.resolve({success: false});
            });

        return deferred.promise;
    }

    this.update_UserProfile = function(new_user_profile) {
        var update_user_profile = $resource(utils.rest.update_User);
        var deferred = $q.defer();
        console.log(new_user_profile.address_id);
        update_user_profile.save({first_name: new_user_profile.first_name, last_name: new_user_profile.last_name,
            email : new_user_profile.email, phone_num : new_user_profile.phone_num,
            address: new_user_profile.address, details: new_user_profile.details})
            .$promise.then(function (result) {
                debugger
                if (result.success == true) {
                    var update_user_address = $resource(utils.rest.update_Address);
                    update_user_address.save({address_id: new_user_profile.address_id, city: new_user_profile.city,
                        state: new_user_profile.state}).
                        $promise.then(function (rst) {
                            if (rst.success == true){
                                var cur_user = {
                                    email: new_user_profile.email,
                                    password: new_user_profile.password
                                }
                                $cookieStore.put('cur_user', cur_user);
                                deferred.resolve({ success: true, message : 'User Update Success!'});
                            }
                        }
                    );
                }
            }, function (error) {
                deferred.resolve({success: false});
            });

        return deferred.promise;
    }

    this.update_UserPhoto = function(myphoto){
        var update_user_photo = $resource(utils.rest.update_Photo);
        var cur_user = $cookieStore.get('cur_user');
        var deferred = $q.defer();
        update_user_photo.save({email: cur_user.email, user_photo_data: myphoto.photo_data}).$promise.then(function (rst){
            if (rst.success == true){
                deferred.resolve({success: true})
            }
        }, function (error){
            deferred.resolve({success: false});
        });
        return deferred.promise;
    }
}]);