﻿var Ticket_on_Cloud = angular.module('Ticket_on_Cloud', ['ngRoute', 'ngCookies', 'ngResource', 'cgNotify', 'react', 'ImageCropper']);

Ticket_on_Cloud.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.
        when('/signup', { controller: 'UserManagementController', templateUrl: '/angularScripts/user/view/signUp.html' }).
        when('/signin', { controller: 'UserManagementController', templateUrl: '/angularScripts/user/view/signIn.html' }).
        when('/userupdate', {controller: 'UserManagementController', templateUrl: '/angularScripts/user/view/update.html'}).
        when('/featured', {controller: 'featuredController', templateUrl: '/angularScripts/featured/view.html'}).
        when('/concert', {controller: 'concertController', templateUrl: '/angularScripts/concert/view.html'}).
        when('/cinema', {controller: 'cinemaController', templateUrl: '/angularScripts/cinema/view.html'}).
        when('/bus', {controller: 'busController', templateUrl: '/angularScripts/bus/view.html'}).
        when('/airlines', {controller: 'airlinesController', templateUrl: '/angularScripts/airlines/view.html'}).
        when('/conferences', {controller: 'conferencesController', templateUrl: '/angularScripts/conferences/view.html'}).
        when('/components', {controller: 'componentsController', templateUrl: '/angularScripts/components/view.html'}).
        otherwise({ redirectTo: '/featured' });
}])
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        $rootScope.signup = function() {
            $location.path('/signup');
        }
        $rootScope.signin = function() {
            $location.path('/signin');
        }
        $rootScope.userupdate = function() {
            $location.path('/userupdate');
        }
        $rootScope.featured = function() {
            $location.path('/featured');
        }
        $rootScope.concert = function() {
            $location.path('/concert');
        }
        $rootScope.cinema = function() {
            $location.path('/cinema');
        }
        $rootScope.bus = function() {
            $location.path('/bus');
        }
        $rootScope.airlines = function() {
            $location.path('/airlines');
        }
        $rootScope.conferences = function() {
            $location.path('/conferences');
        }
        $rootScope.components = function() {
            $location.path('/components');
        }
    }
]);