﻿var Ticket_on_Cloud = angular.module('Ticket_on_Cloud', ['ngRoute', 'ngCookies', 'ngResource']);

Ticket_on_Cloud.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/signUp', { controller: 'userController', templateUrl: '/partialViews/user/signUp.html' })
    .otherwise({ redirectTo: '/signUp' });

}])
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        //if ($cookieStore.get('userInfo') != undefined)
        //    $rootScope.user = $cookieStore.get('userInfo') ;
        //else
        //    $rootScope.user = {};

        //$rootScope.$on('$locationChangeStart', function (event, next, current) {            
        //    if ($rootScope.user.sessionId == undefined) {
        //        $location.path('/');
        //        $('#Header').addClass('hide-header');
        //    }
        //});
    }
]);