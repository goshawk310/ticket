﻿salesPortal.service('loginService', ['$rootScope', '$q', '$resource', '$cookieStore', 'loginFactory', function ($rootScope, $q, $resource, $cookieStore, loginFactory) {

 this.login = function (username, password) {      
     
        var deferred = $q.defer();

        loginFactory.get({ username: username, password: password })
    	.$promise.then(function (result) {
    	    if (result.loginSucceeded) {
    	        $rootScope.user = { sessionId: result.sessionId, username: username };
    	        $cookieStore.put('userInfo', $rootScope.user);
    	        $cookieStore.put('classValue', 1);
    	        deferred.resolve({ success: true });
    	    } else {
    	        deferred.resolve({ success: false });
    	    }
    	});

        return deferred.promise;

    }
}]);