﻿salesPortal.service('logoutService', ['$rootScope', '$q', '$resource', '$cookieStore', 'logoutFactory', function ($rootScope, $q, $resource, $cookieStore, logoutFactory) {
 this.logout = function () {
     var deferred = $q.defer();
     
      logoutFactory.get({ sessionid: $rootScope.user.sessionId })
        .$promise.then(function (result) {
            
            if (result.response == 'SUCCESS') {

                $rootScope.user = {};
                $cookieStore.remove('userInfo');
                $cookieStore.remove('classValue');
                deferred.resolve({ success: true });
            } else {
                deferred.resolve({ success: false });
            }
        });

        return deferred.promise;

    }
}])