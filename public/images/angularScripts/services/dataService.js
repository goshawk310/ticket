﻿salesPortal.service('dataService', ['$rootScope', '$q', '$resource', '$cookieStore', 'salePerSalemanFactory', 'salePerMonthFactory', 'topSalesManFactory', 'topSalesOrderFactory', function ($rootScope, $q, $resource, $cookieStore, salePerSalemanFactory, salePerMonthFactory, topSalesManFactory, topSalesOrderFactory) {

    this.totalSalePerMan = function () {
        var deferred = $q.defer();
        salePerSalemanFactory.get({ sessionid: $rootScope.user.sessionId })
    	.$promise.then(function (result) {            
    	    if (result.resultDescription == 'SUCCESS') {
    	        deferred.resolve({ success: true, data: getPieChartData(result.data) });
    	    } else {
    	        deferred.resolve({ success: false });
    	    }
    	});
        return deferred.promise;

    }
    this.topSalePerMan = function () {
        var deferred = $q.defer();
        topSalesManFactory.get({ sessionid: $rootScope.user.sessionId })
    	.$promise.then(function (result) {
    	    if (result.resultDescription == 'SUCCESS') {
    	        deferred.resolve({ success: true,  data: getBarChartData(result.data)  });
    	    } else {
    	        deferred.resolve({ success: false });
    	    }
    	});
        return deferred.promise;

    }
    this.totalSalePerMonth = function () {
        var deferred = $q.defer();
        salePerMonthFactory.get({ sessionid: $rootScope.user.sessionId })
    	.$promise.then(function (result) {            
    	    if (result.resultDescription == 'SUCCESS') {
    	        
    	        deferred.resolve({ success: true, data: getBarChartData(result.data) });
    	    } else {
    	        deferred.resolve({ success: false });
    	    }
    	});
        return deferred.promise;

    }
    this.topSaleOrder = function () {
        
        var deferred = $q.defer();
        topSalesOrderFactory.get({ sessionid: $rootScope.user.sessionId })
    	.$promise.then(function (result) {
    	    if (result.resultDescription == 'SUCCESS') {

    	        deferred.resolve({ success: true, data: getSaleorderdata(result.data) });
    	    } else {
    	        deferred.resolve({ success: false });
    	    }
    	});
        return deferred.promise;

    }
    var getBarChartData = function (data) {
        var result = [];
        var values = [];
        var x = [];
        var y = [];
        for (var a = 0; a < data.length; a++) {
            x.push(data[a][0]);
            y.push(data[a][1]);
        }
        
        for (var i = 0; i < data.length; i++) {

            values.push([x[i], y[i]]);

            result.push({"key": data[i][0], "values": values })
            values = [];
        }

        return result;
    }
    var getPieChartData = function (data) {
        var result = [];
        for (var i = 0; i < data.length; i++) {
            result.push({ key: data[i][0], value: data[i][1] })
        }

        return result;
    }
    var getSaleorderdata = function (data) {
        var result = [];
        
        for (var i = 0; i < data.length; i++) {
            result.push({ key: data[i].orderNum, username: data[i].userName, value: data[i].value, qty: data[i].qty })
        }

        return result;
    }
    var getTopsaleMan = function (topMan) {
        var result = [];
        var series = [];
        var data = [];
        for (var i = 0; i < topMan.length; i++) {
            //result.push( data[i][0])
            series.push(topMan[i][0]);
            data.push({ x: topMan[i][0], y: [topMan[i][1]] });
        }
        result.push({ series: series, data: data });

        return result;
    }
    
}]);