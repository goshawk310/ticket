﻿salesPortal.factory('loginFactory', function ($resource) {
    return $resource('http://localhost:8080/login');
});
salesPortal.factory('logoutFactory', function ($resource) {
    
    return $resource('http://localhost:8080/logout', { sessionid: '@sessionid' },
        {
            get: {
                method: 'GET',
                transformResponse: function (response) {
                    return { response: response };
                }
            }
        });
});