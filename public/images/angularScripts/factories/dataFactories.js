﻿salesPortal.factory('salePerSalemanFactory', function ($resource) {
    return $resource('http://localhost:8080/salesmandata', { sessionid: '@sessionid' });
});

salesPortal.factory('salePerMonthFactory', function ($resource) {
 
    return $resource('http://localhost:8080/lastyeardata', { sessionid: '@sessionid' });
});

salesPortal.factory('topSalesOrderFactory', function ($resource) {
    
    return $resource('http://localhost:8080/topsalesorders', { sessionid: '@sessionid' });
});
salesPortal.factory('topSalesManFactory', function ($resource) {
    
    return $resource('http://localhost:8080/topsalesmen', { sessionid: '@sessionid' });
});
