﻿salesPortal.controller('TopSalesOrdersController', ['$scope', '$rootScope', '$location', 'dataService', function ($scope, $rootScope, $location, dataService) {

    $scope.hideSaleOrder = false;

    $scope.totalSaleOrder = function () {
        dataService.topSaleOrder().then(function (result) {
            if (result.success) {
                
                $scope.SaleOrderdata = result.data;

            }
        });
    }

    $scope.xFunction = function () {
        return function (d) {
            return d.key;
        };
    }

    $scope.yFunction = function () {
        return function (d) {
            return d.value;
        };
    }

    $scope.Hide = function (value) {

        $scope.hideSaleOrder = true;

    }
    $scope.toolTipContentFunction = function () {
        return function (key, x, y, e, graph) {
            return 'Super New Tooltip' +
                '<p>Order Num ' + '<strong>' + key + '</strong></p>' +
                '<p> value ' + '<strong>' + x + '</strong></p>' +
            '<p> saleman ' + '<strong>' + y.point.username + '</strong></p>' +
                  '<p> saleman ' + '<strong>' + y.point.qty + '</strong></p>'
        }
    }

    $scope.totalSaleOrder();

}]);