﻿salesPortal.controller('SalesPerMonthController', ['$scope', '$rootScope', '$location', 'dataService', function ($scope, $rootScope, $location, dataService) {

    $scope.hideSalePerMonth = false;

    $scope.Hide = function () {

      $scope.hideSalePerMonth = true;               

    }

    $scope.totalSalePerMonth = function () {
        dataService.totalSalePerMonth().then(function (result) {
            if (result.success) {
                $scope.SaleperMonth = [];
                $scope.SaleperMonth = result.data;
            }
        });
    }

    $scope.yAxisTickFormatFunction = function () {
        return function (total) {
            return parseInt(total);
        };
    }

    $scope.totalSalePerMonth();
}])