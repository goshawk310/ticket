﻿salesPortal.controller('homeController', ['$scope', '$rootScope', '$location', 'dataService', function ($scope, $rootScope, $location, dataService) {
    $('#Header').removeClass('hide-header');
    $scope.hidePersaleman = false;
    $scope.hideSalePerMonth = false;
    $scope.hideSaleOrder = false;
    $scope.hideTopSaleMan = false;
    var colorArray = ['#000000', '#660000', '#CC0000', '#FF6666', '#FF3333', '#FF6666', '#FFE6E6'];
  
    $scope.totalSaleOrder = function () {
        dataService.topSaleOrder().then(function (result) {
            if (result.success) {
              
                $scope.SaleOrderdata = result.data;

            }
        });
    }
   
    $scope.salePerSaleMan = function () {
        dataService.totalSalePerMan().then(function (result) {
            if (result.success) {
                $scope.Salemandata = [];
                $scope.Salemandata = result.data;
                
            } 
        });
    }

    $scope.totalSalePerMonth = function () {
        dataService.totalSalePerMonth().then(function (result) {
            if (result.success) {
                $scope.SaleperMonth = [];
                $scope.SaleperMonth = result.data;
            }
        });
    }

    $scope.topSaleMan = function () {
        dataService.topSalePerMan().then(function (result) {
            if (result.success) {
                
                $scope.TopMan =  result.data ;
               
            }
        });
    }

    $scope.colorFunction = function () {
        return function (d, i) {
            return colorArray[i];
        };
    }

    $scope.xFunction = function () {
        return function (d) {
            return d.key;
        };
    }

    $scope.yFunction = function () {
        return function (d) {
            return d.value;
        };
    }

    $scope.Hide = function (value) {
        switch(value) {
            case 1:
                $scope.hidePersaleman = true;
                break;
            case 2:
                $scope.hideSalePerMonth = true;
                break;
            case 3:
                $scope.hideSaleOrder = true;
                break;
            case 4:
                $scope.hideTopSaleMan = true;
                break;
        }
     
    }

    $scope.salePerSaleMan();

    $scope.totalSalePerMonth();

    $scope.yAxisTickFormatFunction = function () {
        return function (total) {            
            return parseInt(total);
        };
    }

    $scope.totalSaleOrder();

    $scope.topSaleMan();

    $scope.toolTipContentFunction = function () {
        return function (key, x, y, e, graph) {
            
            return 'Super New Tooltip' +
                '<p>Order Num ' + '<strong>' + key + '</strong></p>' +
                '<p> value ' + '<strong>' + x + '</strong></p>' +
            '<p> saleman ' + '<strong>' + y.point.username + '</strong></p>' +
                  '<p> saleman ' + '<strong>' + y.point.qty + '</strong></p>'
        }
    }



}])