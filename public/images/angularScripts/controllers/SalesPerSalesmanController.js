﻿salesPortal.controller('SalesPerSalesmanController', ['$scope', '$rootScope', '$location', 'dataService', function ($scope, $rootScope, $location, dataService) {
    $('#Header').removeClass('hide-header');
    $scope.hidePersaleman = false;

    $scope.salePerSaleMan = function () {
        dataService.totalSalePerMan().then(function (result) {
            if (result.success) {
                $scope.Salemandata = [];
                $scope.Salemandata = result.data;

            }
        });
    }
    $scope.xFunction = function () {
        return function (d) {
            return d.key;
        };
    }

    $scope.yFunction = function () {
        return function (d) {
            return d.value;
        };
    }

    $scope.Hide = function () { $scope.hidePersaleman = true; }

    $scope.salePerSaleMan();
}]);