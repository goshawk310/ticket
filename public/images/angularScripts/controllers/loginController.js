﻿salesPortal.controller('loginController', ['$scope', '$rootScope', '$location', 'loginService', function ($scope, $rootScope, $location, loginService) {
    $scope.showMessage = false;
    $scope.$parent.username = $rootScope.user.username;
    
    $scope.login = function () {
        loginService.login($scope.username, $scope.password).then(function (result) {
            
            if (result.success == true) {
                $location.path('/home');
                $scope.$parent.username = $rootScope.user.username;
            } else {
                $scope.showMessage = true;
                $scope.message = "Username or Password is incorrect ";
            }
        });
    }

}])