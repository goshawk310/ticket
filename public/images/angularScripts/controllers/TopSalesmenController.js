﻿salesPortal.controller('TopSalesmenController', ['$scope', '$rootScope', '$location', 'dataService', function ($scope, $rootScope, $location, dataService) {

    $scope.hideTopSaleMan = false;

    $scope.topSaleMan = function () {
        dataService.topSalePerMan().then(function (result) {
            if (result.success) {

                $scope.TopMan = result.data;

            }
        });
    }

    $scope.Hide = function () {

        $scope.hideTopSaleMan = true;

    }

    $scope.yAxisTickFormatFunction = function () {
        return function (total) {
            return parseInt(total);
        };
    }

    $scope.toolTipContentFunction = function () {
        return function (key, x, y, e, graph) {

            return 'Super New Tooltip' +
                '<p>Order Num ' + '<strong>' + key + '</strong></p>' +
                '<p> value ' + '<strong>' + x + '</strong></p>' +
            '<p> saleman ' + '<strong>' + y.point.username + '</strong></p>' +
                  '<p> saleman ' + '<strong>' + y.point.qty + '</strong></p>'
        }
    }

    $scope.topSaleMan();  

}]);