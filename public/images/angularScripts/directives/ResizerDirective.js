﻿salesPortal.directive('resizable', function () {
    return {
        restrict: 'A',
        scope: {
            callback: '&onResize'
        },
        link: function postLink(scope, elem, attrs) {
            elem.resizable();
            elem.on('resizestop', function (evt, ui) {
                if (scope.callback) { scope.callback(); }
            });
        }
    };
});

//salesPortal.directive("ngResize", function ($document) {
//    return function($scope, $element, $attr) {
//        //Reference to the original 
//        var $mouseDown; 

//        // Function to manage resize up event
//        var resizeUp = function($event) {
//            var margin = 50,
//                lowest = $mouseDown.top + $mouseDown.height - margin,
//                top = $event.pageY > lowest ? lowest : $event.pageY,
//                height = $mouseDown.top - top + $mouseDown.height;

//            $element.css({
//                top: top + "px",
//                height: height + "px"
//            });
//        };

//        // Function to manage resize right event
//        var resizeRight = function($event) {
//            var margin = 50,
//                leftest = $element[0].offsetLeft + margin,
//                width = $event.pageX > leftest ? $event.pageX - $element[0].offsetLeft : margin;

//            $element.css({
//                width: width + "px"
//            });
//        };

//        // Function to manage resize down event
//        var resizeDown = function($event) {
//            var margin = 50,
//                uppest = $element[0].offsetTop + margin,
//                height = $event.pageY > uppest ? $event.pageY - $element[0].offsetTop : margin;

//            $element.css({
//                height: height + "px"
//            });
//        };

//        // Function to manage resize left event
//        function resizeLeft ($event) {
//            var margin = 50,
//                rightest = $mouseDown.left + $mouseDown.width - margin,
//                left = $event.pageX > rightest ? rightest : $event.pageX,
//                width = $mouseDown.left - left + $mouseDown.width;        

//            $element.css({
//                left: left + "px",
//                width: width + "px"
//            });
//        };

//        var createResizer = function createResizer( className , handlers ){

//            newElement = angular.element( '<div class="' + className + '"></div>' );
//            $element.append(newElement);
//            newElement.on("mousedown", function($event) {

//                $document.on("mousemove", mousemove);
//                $document.on("mouseup", mouseup);

//                //Keep the original event around for up / left resizing
//                $mouseDown = $event;
//                $mouseDown.top = $element[0].offsetTop;
//                $mouseDown.left = $element[0].offsetLeft
//                $mouseDown.width = $element[0].offsetWidth;
//                $mouseDown.height = $element[0].offsetHeight;                

//                function mousemove($event) {
//                    event.preventDefault();
//                    for( var i = 0 ; i < handlers.length ; i++){
//                        handlers[i]( $event );
//                    }
//                }

//                function mouseup() {
//                    $document.off("mousemove", mousemove);
//                    $document.off("mouseup", mouseup);
//                }         
//            });
//        }

//        createResizer( 'sw-resize' , [ resizeDown , resizeLeft ] );
//        createResizer( 'ne-resize' , [ resizeUp   , resizeRight ] );
//        createResizer( 'nw-resize' , [ resizeUp   , resizeLeft ] );
//        createResizer( 'se-resize' , [ resizeDown ,  resizeRight ] );
//        createResizer( 'w-resize' , [ resizeLeft ] );
//        createResizer( 'e-resize' , [ resizeRight ] );
//        createResizer( 'n-resize' , [ resizeUp ] );
//        createResizer( 's-resize' , [ resizeDown ] );
//    };

//});
