﻿$(document).ready(function () {

    $('.menu-toggle').on('click', function () {
        $('nav').toggleClass('active-nav');
        $('.menu-toggle').toggleClass('menu-icon-active');
        if($('.menu-toggle').hasClass('menu-icon-active'))
        {
            $('.menu-toggle i').removeClass('glyphicon-menu-hamburger').addClass('glyphicon-remove');
        }
        else
        {
            $('.menu-toggle i').removeClass('glyphicon-remove').addClass('glyphicon-menu-hamburger');
        }
    });
});