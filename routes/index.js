var exports = module.exports = {};

var express = require('express');

/* dbcreate */
var db_config_util = require("../api/das/index.js");

var service = require('../api/service'),
    auth = require('../api/utility/auth.js'),
    UserManagementService = service.UserManagementService(),
    eventService = service.EventService();

exports.registerRouters = function(app) {
    /*
     *   type : POST
     *   url : http://localhost:8182/signin
     *   Content-Type : application/json
     *   request body : {"email":"{email}","password":"{password}"}
     */

    app.post( '/user/signin', function(request, response ) {
        console.log("-------signin = " + request.body.email + ":" + request.body.password + "--------");
        var user = {
            email: request.body.email,
            password: request.body.password
        };

        UserManagementService.authenticate(user,response);
    });

    /*
     *   type : POST
     *   url : http://localhost:8182/user/signup
     *   Content-Type : application/json
     *   request body : {"email":"{email}","password":"{password}","firstName":"{firstName}","lastName":"{lastName}"}
     */
    app.post( '/user/signup', function(request, response ) {
        var user = {
            email: request.body.email,
            password: request.body.password,
            firstName: request.body.firstName,
            lastName: request.body.lastName
        };

        UserManagementService.addUser(user, response);
    });

    /*
     * type: POST
     * url : http://localhost:8182/user/get_userProfile
     * Content-Type : application/json
     * requrest body : {"email" : "email"}
     */

    app.post( '/user/get_userprofile', function(request, response) {
        console.log("-------get_userProfile------" + request.body.email);
        var user_profile = {
            email: request.body.email,
            password: request.body.password
        };
        UserManagementService.get_userProfile(user_profile, response);
    });

    /*
     *   type : GET
     *   url : http://localhost:8182/user/verify?code={code}
     */
    app.get( '/user/verify', function( request, response ) {
        var code = request.query.code
        UserManagementService.confirm(code, response);
    });
    /*
     *   type : GET
     *   url : http://localchost:8182/user/get?authToken={token}
     */
    app.get( '/user/get', [auth], function( request, response ) {
        var id = request.user;
        //if this is not a number
        if(isNaN(id) == true)
        {
            id = 0;
        }
        UserManagementService.findById(id, response);
    });

    /*
     *   type : GET
     *   url : http://localhost:8182/user/delete?authToken={token}
     */

    app.post( '/user/address/update', function(request, response ) {
        console.log('------------------update_address : ' + request.body.address_id + request.body.city + request.body.state);
        var address = {
            userId : request.body.address_id,
            city: request.body.city,
            state: request.body.state//,
            //country: request.body.country
        };

        UserManagementService.updateAddress(address,response);
    });


    /*
     *   type : POST
     *   url : http://localhost:8182/user/update?authToken={token}
     *   Content-Type : application/json
     *   request body : {"email":"{email}","password":"{password}","firstName":"{firstName}","lastName":"{lastName}"}
     */
    app.post( '/user/update_user', function(request, response ) {
        console.log('------------------update_user : ' + request.body.phone_num)
        var user = {
            email: request.body.email,
            phone_num: request.body.phone_num,
            details: request.body.details,
            first_name: request.body.first_name,
            last_name: request.body.last_name
        };
        //UserManagementService.updateAddress(address, response);
        UserManagementService.updateUser(user,response);
    });

    app.post( '/user/update_photo', function(request, response){
        console.log('------------update_photo');
        var user_photo = {
            email: request.body.email,
            photo_data: request.body.user_photo_data
        }
        UserManagementService.update_UserPhoto(user_photo, response);
    });
}
